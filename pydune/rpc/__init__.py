from pydune.rpc.shell import *
from pydune.rpc.protocol import *
from pydune.rpc.helpers import *
from pydune.rpc.search import *
from pydune.rpc.node import RpcNode


class RpcProvider:

    def __init__(self, **urls):
        self.urls = urls

    @lru_cache(maxsize=None)
    def __getattr__(self, network) -> ShellQuery:
        return ShellQuery(node=RpcNode(uri=self.urls[network], network=network))

    def __dir__(self):
        return list(super(RpcProvider, self).__dir__()) + list(self.urls.keys())

    def __repr__(self):
        res = [
            super(RpcProvider, self).__repr__(),
            '\nNetworks',
            *list(map(lambda x: f'.{x[0]}  # {x[1]}', self.urls.items()))
        ]
        return '\n'.join(res)


localhost = RpcProvider(
    mainnet='https://127.0.0.1:8732/',
    testnet='https://127.0.0.1:8732/',
    devnet='https://127.0.0.1:8732/'
)
dunscan = RpcProvider(
    mainnet='https://mainnet-node.dunscan.io/',
    testnet='https://testnet-node.dunscan.io/',
    devnet='https://devnet-node.dunscan.io/'
)
tzbeta = RpcProvider(
    mainnet='https://rpc.tzbeta.net/',
    testnet='https://rpcalpha.tzbeta.net/'
)
tezbox = RpcProvider(
    mainnet='https://rpc.tezrpc.me/',
    testnet='https://alphanet.tezrpc.me/'
)
cryptonomic = RpcProvider(
    mainnet='https://tezos-prod.cryptonomic-infra.tech/',
    testnet='https://tezos-dev.cryptonomic-infra.tech/'
)
tulip = RpcProvider(
    mainnet='https://rpc.tulip.tools/mainnet/',
    testnet='https://rpc.tulip.tools/alphanet/',
    devnet='https://rpc.tulip.tools/zeronet/'
)
letzbake = RpcProvider(
    mainnet='https://teznode.letzbake.com:443/'
)

mainnet = dunscan.mainnet
testnet = dunscan.testnet.alphanet
devnet = dunscan.devnet
